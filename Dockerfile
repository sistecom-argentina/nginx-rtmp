FROM nginx:1.24.0-alpine
RUN sed -i 's/v3.17/v3.19/g' /etc/apk/repositories
RUN mkdir -p /var/www/html/stream && chown nginx /var/www/html/stream
RUN apk del nginx* && apk add --no-cache nginx nginx-mod-rtmp
ADD config/nginx.conf /etc/nginx/nginx.conf
EXPOSE 80/tcp
EXPOSE 1935/tcp

